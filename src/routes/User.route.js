/* eslint-disable no-underscore-dangle */
const express = require('express');

const Auth = require('../middleware/AuthLogin');
const {
  removeUser, showUser, searchUser, postUpdateProfile,
  updateProfilePhoto, savedPost, savePost, unsavePost,
} = require('../service/User');
const { editAccountValidation } = require('../validations/User.validation');

const router = express.Router();

router.get('/show-user', Auth, async (req, res) => {
  try {
    const response = await showUser(req.user._id);

    return res.status(response.status).json({
      message: response.message,
      res: response.res,
      User: response.User,
    });
  } catch (err) {
    res.status(500).json({
      message: 'Controller Error Occured',
      Error: err,
    });
  }
});
router.get('/remove-user', Auth, async (req, res) => {
  try {
    const response = await removeUser(req.user._id);

    return res.status(response.status).json({
      message: response.message,
      res: response.res,
    });
  } catch (err) {
    res.json({
      message: 'Controller Error Occured',
    });
  }
});
router.post('/user/edit-account', Auth, editAccountValidation, async (req, res) => {
  const updatedData = {
    Email: req.body.email,
    UserName: req.body.userName,
    FullName: req.body.fullName,
    Phone: req.body.phone,
    Bio: req.body.bio,
    Gender: req.body.gender,
  };
  try {
    const response = await postUpdateProfile(req.user._id, updatedData);

    return res.status(response.status).json({
      message: response.message,
      res: response.res,
    });
  } catch (err) {
    res.status(500).json({
      message: 'User detail Updation Error',
    });
  }
});
router.get('/search-user', Auth, async (req, res) => {
  try {
    const { userName } = req.params;

    const response = await searchUser(userName);

    return res.status(response.status).json({
      message: response.message,
      res: response.res,
      User: response.User,
    });
  } catch (err) {
    res.status(500).json({
      message: 'An Error Occured!',
    });
  }
});
router.post('/user/edit-profile-photo', Auth, async (req, res) => {
  try {
    const image = req.file.path;
    const updatedData = {
      ProfilePicture: image,
    };
    const response = await updateProfilePhoto(req.user._id, updatedData);

    return res.status(response.status).json({
      message: response.message,
      res: response.res,
    });
  } catch (err) {
    res.status(500).json({
      message: 'User Profile Photo Updation Error!',
    });
  }
});

router.get('saved-post', async (req, res) => {
  try {
    const response = await savedPost(req.user._id);
    return res.status(response.status).json({
      res: response.res,
      message: response.message,
    });
  } catch (err) {
    res.status(500).json({
      message: 'saved post getting error',
    });
  }
});

router.put('/user/save-post/:postId', Auth, async (req, res) => {
  try {
    const response = await savePost(req.params.postId, req.user_id);
    return res.status(response.status).json({
      message: response.message,
      res: response.res,
    });
  } catch (err) {
    return res.status(500).json({
      Error: err,
    });
  }
});

router.put('/user/unsave-post/:postId', Auth, async (req, res) => {
  try {
    const response = await unsavePost(req.params.postId, req.user_id);
    return res.status(response.status).json({
      message: response.message,
      res: response.res,
    });
  } catch (err) {
    return res.status(500).json({
      Error: err,
    });
  }
});

module.exports = router;
