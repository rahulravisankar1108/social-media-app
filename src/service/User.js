const User = require('../models/User.model');

exports.showUser = async (userId) => {
  try {
    const userDetails = await User.findById(userId);
    if (userDetails) {
      const response = {
        User: userDetails,
        status: 200,
        message: 'user Details',
        res: true,
      };
      return response;
    }
    const response = {
      status: 422,
      message: 'No User available',
      res: false,
      User: 'Error',
    };
    return response;
  } catch (err) {
    const response = {
      status: 500,
      message: 'Error Occured!',
      res: false,
      User: err,
    };
    return response;
  }
};

exports.removeUser = async (userId) => {
  try {
    const DeletingUser = await User.findById(userId, {
      Followers: 1, Following: 1, Post: 1, Request: 1, GivenRequest: 1,
    });
    if (DeletingUser) {
      const DeletingFollowers = DeletingUser.Followers;
      const DeletingFollowing = DeletingUser.Following;
      const DeletingPost = DeletingUser.Post;
      const DeletingRequest = DeletingUser.Request;
      const DeletingGivenRequest = DeletingUser.GivenRequest;
      if (DeletingFollowers.length > 0) {
        DeletingFollowers.map(async (friend) => {
          await User.findOneAndUpdate(
            { _id: friend },
            { $pull: { Following: userId } },
            { safe: true, upsert: true },
          );
        });
      }
      if (DeletingFollowing.length > 0) {
        DeletingFollowing.map(async (friend) => {
          await User.findOneAndUpdate(
            { _id: friend },
            { $pull: { Followers: userId } },
            { safe: true },
          );
        });
      }
      if (DeletingRequest.length > 0) {
        DeletingRequest.map(async (friend) => {
          await User.findOneAndUpdate(
            { _id: friend },
            { $pull: { GivenRequest: userId } },
            { safe: true },
          );
        });
      }
      if (DeletingGivenRequest.length > 0) {
        DeletingGivenRequest.map(async (friend) => {
          await User.findOneAndUpdate(
            { _id: friend },
            { $pull: { Request: userId } },
            { safe: true },
          );
        });
      }
      if (DeletingPost.length > 0) {
        DeletingPost.map(async (post) => {
          await User.findOneAndUpdate(
            { _id: post },
            { $set: { Post: [] } },
            { safe: true, new: true },
          );
        });
      }

      const response = {
        status: 200,
        message: 'User Deleted Succesfully',
        res: true,
      };
      return response;
    }
  } catch (err) {
    const response = {
      status: 500,
      message: err,
      res: false,
    };
    return response;
  }
};

exports.postupdateProfile = async (userId, updatedData) => {
  try {
    const updateProfile = await User.findByIdAndUpdate(userId,
      { $set: updatedData },
      { new: true });
    if (updateProfile) {
      const response = {
        status: 200,
        message: 'Updated Profile',
        res: true,
      };
      return response;
    }

    const response = {
      status: 422,
      message: 'Updation Error',
      res: false,
    };
    return response;
  } catch (err) {
    const response = {
      status: 500,
      message: 'User detail Updation Error',
      res: false,
    };
    return response;
  }
};

exports.searchUser = async (userName) => {
  try {
    const FindUser = await User.find({ UserName: userName });
    if (FindUser) {
      const response = {
        status: 200,
        User: FindUser,
        message: 'User Details',
        res: true,
      };
      return response;
    }
    const response = {
      User: 'Error',
      status: 422,
      message: 'User not found',
      res: false,
    };
    return response;
  } catch (err) {
    const response = {
      User: err,
      status: 500,
      message: 'User finding Error',
      res: false,
    };
    return response;
  }
};

exports.updateProfilePhoto = async (userId, updatedData) => {
  try {
    const updatedPhoto = await User.findByIdAndUpdate(userId,
      { $set: updatedData }, { new: true, safe: true });
    if (updatedPhoto) {
      const response = {
        status: 200,
        message: 'User Profile Photo Updated!',
        res: true,
      };
      return response;
    }
    const response = {
      status: 422,
      message: 'User Profile not updated!',
      res: false,
    };
    return response;
  } catch (err) {
    const response = {
      status: 500,
      message: `User Profile Photo Updation Error! - ${err}`,
      res: false,
    };
    return response;
  }
};

exports.savedPost = async (userId) => {
  try {
    const post = await User.findById(userId)
      .populate('savedPosts', '_id');
    if (post) {
      const response = {
        status: 200,
        message: post,
        res: true,
      };
      return response;
    }
    const response = {
      status: 201,
      message: 'No saved post',
      res: true,
    };
    return response;
  } catch (err) {
    const response = {
      status: 500,
      message: err,
      res: false,
    };
    return response;
  }
};

exports.savePost = async (postId, userId) => {
  try {
    const updatedSavedPost = await User.findByIdAndUpdate(userId,
      { $push: { savedPosts: postId } }, { new: true, safe: true });
    if (updatedSavedPost) {
      const response = {
        status: 200,
        Message: 'saved!',
        res: true,
      };
      return response;
    }
  } catch (err) {
    const response = {
      status: 500,
      Message: err,
      res: false,
    };
    return response;
  }
};

exports.unsavePost = async (postId, userId) => {
  try {
    const updatedSavedPost = await User.findByIdAndUpdate(userId,
      { $pull: { savedPosts: postId } }, { new: true, safe: true });
    if (updatedSavedPost) {
      const response = {
        Message: 'Unsaved!',
        res: true,
        status: 200,
      };
      return response;
    }
    const response = {
      Message: 'Not unsaved!',
      res: false,
      status: 422,
    };
    return response;
  } catch (err) {
    const response = {
      Message: err,
      res: false,
      status: 500,
    };
    return response;
  }
};
