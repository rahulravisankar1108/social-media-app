const User = require('../models/User.model');

exports.removeUser = async (userId, GivenRequestUser) => {
  try {
    await User.updateOne(
      { _id: userId },
      { $pull: { GivenRequest: GivenRequestUser } },
      { safe: true },
    );
    await User.updateOne({ _id: GivenRequestUser }, { $pull: { Request: userId } }, { safe: true });
    const response = {
      status: 200,
      Message: 'Deleted the Given Request ID',
      res: true,
    };
    return response;
  } catch (err) {
    const response = {
      status: 500,
      Message: 'Deletion Error',
      res: false,
    };
    return response;
  }
};

exports.showUsers = async (userId) => {
  try {
    const currentUser = await User.findById(userId, { GivenRequest: 1 })
      .populate('GivenRequest', '_id UserName Email Phone Bio Website Gender ProfilePicture');
    if (currentUser) {
      const response = {
        status: 200,
        RequestedUsers: currentUser,
        res: true,
      };
      return response;
    }
    const response = {
      status: 400,
      RequestedUsers: null,
      res: false,
    };
    return response;
  } catch (err) {
    const response = {
      status: 500,
      RequestedUsers: null,
      res: false,
    };
    return response;
  }
};

exports.countUsers = async (userId) => {
  try {
    const currentUser = await User.findById(userId, { GivenRequest: 1 });
    const count = currentUser.GivenRequest.length;
    const response = {
      Message: 'Given requests Count',
      res: true,
      status: 200,
      GivenRequestCount: count,
    };
    return response;
  } catch (err) {
    const response = {
      Message: 'Count error occured',
      res: false,
      status: 500,
      GivenRequestCount: null,
    };
    return response;
  }
};
