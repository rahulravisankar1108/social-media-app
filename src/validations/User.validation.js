const User = require('../models/User.model');

exports.editAccountValidation = (req, res, next) => {
  const {
    userId, userName, email, fullName, phone, bio, gender,
  } = req.body;

  if (!bio || !gender || !phone || !email || !userName || !fullName) {
    return res.status(422).json({
      Error: 'Please fill all the fields.',
    });
  }

  const isValidUser = userName.length >= 4;
  const isValidName = fullName.length >= 3;
  const isValidPhone = phone.match(/^[6-9]\d{9}$/);
  const isValidEmail = email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);

  if (!isValidPhone) {
    return res.status(422).json({ Error: 'Enter a valid Phone Number' });
  }

  if (!isValidEmail) {
    return res.status(422).json({ Error: 'Please enter a valid Email' });
  }

  if (!isValidName) {
    return res.status(422).json({ Error: 'Name should atleast be 3 characters long' });
  }

  if (!isValidUser) {
    return res.status(422).json({ Error: 'Username should atleast be 4 characters long' });
  }

  User.findOne({ UserName: userName }).then((user) => {
    if (user) {
      if (user.UserName === userName && user._id !== userId) {
        return res.status(422).json({ Message: 'UserName already taken.' });
      }
      User.findOne({ Email: email }).then((userss) => {
        if (userss) {
          if (userss.Email === email && userss._id !== userId) {
            return res.status(422).json({ Message: 'Email address already exists' });
          }
        }
        next();
      });
    }
    next();
  });
};
