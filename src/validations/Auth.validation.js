const bcrypt = require('bcrypt');

const User = require('../models/User.model');

exports.signUpValidation = (req, res, next) => {
  const {
    userName,
    fullName,
    email,
    phone,
    password,
    confirmPassword,
  } = req.body;

  if (!phone || !email || !password || !userName || !fullName || !confirmPassword) {
    return res.status(422).json({
      Error: 'Please fill all the fields.',
    });
  }

  const isLengthValid = password.length >= 6 && confirmPassword.length >= 6;
  const isValidUser = userName.length >= 4;
  const isValidName = fullName.length >= 3;
  const isValidPhone = phone.match(/^[6-9]\d{9}$/);
  const isEqualPassword = password === confirmPassword;
  const isValidEmail = email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);

  if (!isLengthValid) {
    return res.status(422).json({ Error: 'Password and confirm Password must atleast be 6 characters long.' });
  }

  if (!isValidPhone) {
    return res.status(422).json({ Error: 'Enter a valid Phone Number' });
  }

  if (!isValidEmail) {
    return res.status(422).json({ Error: 'Please enter a valid Email' });
  }

  if (!isEqualPassword) {
    return res.status(422).json({ Error: 'Password do not match' });
  }

  if (!isValidName) {
    return res.status(422).json({ Error: 'Name should atleast be 3 characters long' });
  }

  if (!isValidUser) {
    return res.status(422).json({ Error: 'Username should atleast be 4 characters long' });
  }

  User.findOne({ Email: email }).then((user) => {
    if (user) {
      const message = 'Email address already exists';
      res.status(422).json({ Error: message });
    }

    User.findOne({ UserName: userName }).then((userr) => {
      if (userr) {
        const message = 'UserName already exists';
        res.status(422).json({ Error: message });
      }

      next();
    });
  });
};

exports.loginValidation = (req, res, next) => {
  const {
    email,
    password,
  } = req.body;

  if (!email || !password) {
    return res.status(422).json({
      Error: 'Please fill all the fields.',
    });
  }

  User.findOne({ Email: email }).then((user) => {
    if (!user) {
      const message = 'Email not registered';
      return Promise.reject(message);
    }
    bcrypt.compare(password, user.Password)
      .then((isEqualPassword) => {
        if (!isEqualPassword) {
          const message = 'Password is incorrect!';
          return Promise.reject(message);
        }
        next();
      });
  })
    .catch(() => res.status(422).json({ error: 'Invalid Email or password' }));
};
