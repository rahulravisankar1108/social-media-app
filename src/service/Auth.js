const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/User.model');

exports.signUp = async (email, userName, fullName, password, phone) => {
  try {
    const salt = await bcrypt.genSalt(12);
    const hashedPassword = await bcrypt.hash(password.toString(), salt);
    const New = new User({
      Email: email,
      UserName: userName,
      FullName: fullName,
      Password: hashedPassword,
      Phone: phone,
      Bio: '',
      Website: '',
      Gender: '',
      ProfilePicture: '',
      Followers: [],
      Following: [],
      Request: [],
      GivenRequest: [],
      Post: [],
    });
    const createdNewUser = await New.save();
    if (createdNewUser) {
      const response = {
        status: 200,
        Message: 'New User Details Added!',
        res: true,
      };
      return response;
    }
  } catch (err) {
    const response = {
      status: 404,
      message: 'Error Occured!',
      res: false,
    };
    return response;
  }
};

exports.login = async (email) => {
  try {
    const currentUser = await User.findOne({ Email: email });

    const token = jwt.sign({ _id: currentUser._id }, process.env.SECRET, { expiresIn: 360000 });

    const response = {
      status: 200,
      Message: 'Welcome User',
      Token: token,
      res: true,
      User: {
        userid: currentUser._id,
        fullName: currentUser.FullName,
        email: currentUser.Email,
        followers: currentUser.Followers,
        following: currentUser.Following,
        request: currentUser.Request,
        givenRequest: currentUser.GivenRequest,
        profilePicture: currentUser.ProfilePicture,
      },
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      Message: 'Unexpected Error Occured!',
      Error: err,
      res: false,
    };
    return response;
  }
};
