const express = require('express');
const Auth = require('../middleware/AuthLogin');

const router = express.Router();

const {
  initiate, removeUser, showUser, countUser, clearUser,
} = require('../service/Request');

router.post('/add-user/:targetId', Auth, async (req, res) => {
  try {
    const response = await initiate(req.params.targetId, req.user._id);

    return res.status(response.status).json({
      message: response.message,
    });
  } catch (err) {
    res.status(404).json({
      message: 'An Error occured in initiate controller',
    });
  }
});

router.post('/remove-user/:requestId', Auth, async (req, res) => {
  try {
    const response = await removeUser(req.user_id, req.params.requestId);
    res.status(response.status).json({
      message: response.message,
    });
  } catch (err) {
    res.status(404).json({
      message: 'Request controller deletion Error',
    });
  }
});

router.get('/show-users', Auth, async (req, res) => {
  try {
    const response = await showUser(req.user_id);

    res.status(response.status).json({
      User: response.User,
      res: response.res,
      message: response.message,
    });
  } catch (err) {
    res.status(500).json({
      message: 'Find user error Occured',
      Error: err,
    });
  }
});

router.get('/count-users', Auth, async (req, res) => {
  try {
    const response = await countUser(req.user_id);
    res.status(response.status).json({
      requestCount: response.Count,
      res: response.res,
      message: response.Count,
    });
  } catch (err) {
    res.json({
      message: 'Controller CountUser error occured',
      Error: err,
    });
  }
});

router.get('/clear-users', Auth, async (req, res) => {
  try {
    const response = await clearUser(req.user_id);
    res.status(response.status).json({
      message: response.message,
      user: response.User,
      res: response.res,
    });
  } catch (err) {
    res.status(500).json({
      Error: err,
    });
  }
});

module.exports = router;
