const express = require('express');
const Auth = require('../middleware/AuthLogin');

const router = express.Router();

const { removeUser, showUsers, countUsers } = require('../service/GivenRequest');

router.post('/remove-user/:givenRequestUser', Auth, async (req, res) => {
  try {
    const response = await removeUser(req.user._id, req.params.givenRequestUser);

    return res.status(response.status).json({
      message: response.message,
      res: response.res,
    });
  } catch (err) {
    res.status(500).json({
      message: 'Deletion Error',
      Error: err,
    });
  }
});
router.get('/show-users', Auth, async (req, res) => {
  try {
    const response = await showUsers(req.user._id);

    return res.status(response.status).json({
      requestedUsers: response.requestedUsers,
      res: response.res,
    });
  } catch (err) {
    res.status(500).json({
      Error: err,
    });
  }
});
router.get('/count-users', Auth, async (req, res) => {
  try {
    const response = await countUsers(req.user._id);
    return res.status(response.status).json({
      givenRequestCount: response.givenRequestCount,
      res: response.res,
      message: response.message,
    });
  } catch (err) {
    res.json({
      message: 'Count error occured',
      Error: err,
    });
  }
});

module.exports = router;
