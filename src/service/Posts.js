const { Posts } = require('../models/Posts.model');
const User = require('../models/User.model');

exports.getFriendsPost = async (userId) => {
  try {
    const FriendPost = await User.findById(userId, { Following: 1 })
      .populate('postedBy', 'Image Caption Location')
      .populate('comments.postedBy', '_id message')
      .populate('comments.likes', 'UserName _id ProfilePhoto');
    if (FriendPost) {
      const response = {
        status: 200,
        friendPost: FriendPost,
        message: 'friend post',
        res: true,
      };
      return response;
    }
    const response = {
      status: 201,
      friendPost: 0,
      message: 'You dont Follow Anyone',
      res: false,
    };
    return response;
  } catch (err) {
    const response = {
      status: 400,
      friendPost: 0,
      message: 'You dont Follow Anyone',
      res: false,
    };
    return response;
  }
};

exports.storePost = async (userKey, location, image, caption) => {
  try {
    const userId = userKey;
    const newPost = new Posts({
      Location: location,
      Caption: caption,
      Image: image,
      postedBy: userId,
      likedBy: [],
      savedBy: [],
      comments: [],
    });

    newPost.save();

    if (newPost) {
      const response = {
        message: 'Post added Successfully',
        status: 200,
        res: true,
      };
      return response;
    }
    const response = {
      message: 'Post add Failed',
      status: 400,
      res: false,
    };
    return response;
  } catch (err) {
    console.log(err);
  }
};

exports.getUserPost = async (ID) => {
  try {
    const post = await Posts.find({ postedBy: ID })
      .populate('postedBy', 'Caption Location Image')
      .populate('comments.postedBy', '_id UserName Email')
      .populate('comments.likes', '_id UserName ProfilePhoto');
    if (post) {
      const response = {
        res: true,
        status: 200,
        postDetails: post,
      };
      return response;
    }
    const response = {
      res: false,
      status: 400,
      postDetails: null,
    };
    return response;
  } catch (err) {
    const response = {
      res: false,
      status: 500,
      postDetails: err,
    };
    return response;
  }
};

exports.updatePostDetails = async (ID, caption, location) => {
  try {
    await Posts.updateOne(
      { _id: ID },
      { $set: { Caption: caption, Location: location } },
      { safe: true, upsert: true },
    );
    const response = {
      message: 'Updated',
      res: true,
      status: 200,
    };
    return response;
  } catch (err) {
    const response = {
      message: 'An error Occured',
      res: false,
      status: 500,
    };
    return response;
  }
};

exports.removePost = async (ID) => {
  try {
    if (await Posts.findByIdAndRemove(ID)) {
      const response = {
        res: true,
        message: 'Post Deleted',
        status: 200,
      };
      return response;
    }
    const response = {
      res: false,
      message: 'Post not deleted',
      status: 404,
    };
    return response;
  } catch (err) {
    const response = {
      res: false,
      status: 500,
      message: err,
    };
    return response;
  }
};

exports.removePosts = async (userId) => {
  try {
    const deleteUserPost = await Posts.deleteMany({ postedBy: userId });
    if (deleteUserPost) {
      const response = {
        message: 'Deleted All Posts',
        res: true,
        status: 200,
      };
      return response;
    }
    const response = {
      message: 'No User Found!',
      res: false,
      status: 404,
    };
    return response;
  } catch (err) {
    const response = {
      message: `Error occured${err}`,
      res: false,
      status: 500,
    };
    return response;
  }
};

exports.likePost = async (postId, userId) => {
  try {
    const updatedLikes = await Posts.findByIdAndUpdate(postId,
      { $push: { likedBy: userId } }, { new: true, safe: true });
    if (updatedLikes) {
      const response = {
        message: 'Liked!',
        res: true,
        status: 200,
      };
      return response;
    }
  } catch (err) {
    const response = {
      message: err,
      res: false,
      status: 500,
    };
    return response;
  }
};

exports.unlikePost = async (postId, userId) => {
  try {
    const updatedLikes = await Posts.findByIdAndUpdate(postId,
      { $pull: { likedBy: userId } }, { new: true, safe: true });
    if (updatedLikes) {
      const response = {
        message: 'unliked!',
        res: true,
        status: 200,
      };
      return response;
    }
  } catch (err) {
    const response = {
      message: err,
      res: false,
      status: 500,
    };
    return response;
  }
};
