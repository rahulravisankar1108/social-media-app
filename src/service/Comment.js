const Comment = require('../models/Comment.model');
const { Posts } = require('../models/Posts.model');

exports.add = async (postId, Message, userId) => {
  try {
    const newComment = new Comment({
      message: Message,
      postedBy: userId,
    });
    newComment.save();
    const updatedComment = await Posts.findByIdAndUpdate(postId,
      { $push: { comments: newComment } }, { new: true, safe: true });

    if (updatedComment) {
      const response = {
        status: 200,
        res: true,
        message: 'Added Comment',
      };
      return response;
    }
    const response = {
      status: 422,
      res: false,
      message: 'Failed to add Comment',
    };
    return response;
  } catch (err) {
    const response = {
      status: 500,
      res: false,
    };
    return response;
  }
};

exports.deleteComment = async (postId, commentId) => {
  try {
    const deleteComment = await Comment.findByIdAndRemove(commentId);
    const updatedPost = await Posts.findByIdAndUpdate(postId,
      { $pull: { comments: commentId } }, { new: true, safe: true });

    if (deleteComment && updatedPost) {
      const response = {
        message: updatedPost,
        res: true,
        status: 200,
      };
      return response;
    }
    const response = {
      message: 'comment not Deleted',
      res: false,
      status: 422,
    };
    return response;
  } catch (err) {
    const response = {
      message: err,
      res: false,
      status: 500,
    };
    return response;
  }
};

exports.like = async (commentId, userId) => {
  try {
    const comment = await Comment.findByIdAndUpdate(commentId, {
      $push: { likes: userId },
    }, {
      new: true,
      safe: true,
    });
    if (comment) {
      const response = {
        status: 200,
        message: comment,
        res: true,
      };
      return response;
    }
    const response = {
      status: 422,
      res: false,
      message: 'comment not found!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 500,
      message: 'Error in like comment',
    };
    return response;
  }
};

exports.unLike = async (commentId, userId) => {
  try {
    const comment = await Comment.findByIdAndUpdate(commentId, {
      $pull: { likes: userId },
    }, {
      new: true, safe: true,
    });
    if (comment) {
      const response = {
        status: 200,
        message: comment,
        res: true,
      };
      return response;
    }
    const response = {
      status: 422,
      res: false,
      message: 'comment not found!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 500,
      res: false,
      message: 'Error in unlike comment',
    };
    return response;
  }
};
