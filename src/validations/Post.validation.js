exports.storePostValidator = (req, res, next) => {
  const { location, caption } = req.body;
  const Image = req.file.path;

  const isLocation = location.length > 0;
  const isCaption = caption.length > 0;

  if (!isLocation || !isCaption || !Image) {
    return res.status(422).json({ Error: 'Please fill all the fields.' });
  }
  next();
};

exports.updatePostValidator = (req, res, next) => {
  const {
    caption,
    location,
  } = req.body;

  const isLocation = location.length > 0;
  const isCaption = caption.length > 0;

  if (!isLocation || !isCaption) {
    return res.status(422).json({ Error: 'Please fill all the fields.' });
  }
  next();
};
