const { LocalStorage } = require('node-localstorage');
const assert = require('assert');
const {
  dbConnect,
  dbDisconnect,
} = require('../utils/test-utils/dbHandler.utils');
require('dotenv').config();

const localStorage = new LocalStorage('./scratch');

const User = require('../../src/models/User.model');

const testId = localStorage.getItem('testId');
let done;
let res;

describe('Unit Tests : Test for Routes/Following.js:', () => {
  beforeEach(() => {
    jest.setTimeout(30000);
  });

  beforeAll(async () => {
    dbConnect();
  });

  afterAll(async () => {
    dbDisconnect();
    await new Promise((resolve) => setTimeout(() => resolve(), 50000));
  });

  it('should start following other user', async () => {
    await User.updateOne({ _id: testId }, {
      $push: { Following: process.env.userId },
    },
    { new: true })
      .select('-Password')
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  }, 50000);

  it('should stop following other user', async () => {
    await User.updateOne({ _id: testId }, {
      $pull: { Following: process.env.userId },
    },
    { new: true })
      .select('-Password')
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  }, 50000);

  it('should get all the following user Details', async () => {
    try {
      const user = await User.findOne(testId);
      if (user) {
        assert.ok(res.statusCode === 200);
        done();
      }
    } catch (err) {
      expect(201);
    }
  });
}, 50000);
