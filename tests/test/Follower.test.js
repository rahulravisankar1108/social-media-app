const { LocalStorage } = require('node-localstorage');
const assert = require('assert');
const {
  dbConnect,
  dbDisconnect,
} = require('../utils/test-utils/dbHandler.utils');
require('dotenv').config();

const localStorage = new LocalStorage('./scratch');

const User = require('../../src/models/User.model');

const testId = localStorage.getItem('testId');
let done;
let res;

describe('Unit Tests : Test for Routes/Follower.js:', () => {
  beforeEach(() => {
    jest.setTimeout(50000);
  });

  beforeAll(async () => {
    dbConnect();
  });

  afterAll(async () => {
    dbDisconnect();
    await new Promise((resolve) => setTimeout(() => resolve(), 10000));
  });

  it('should find the follower of the existing user', () => {
    User.findById(process.env.userId)
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  }, 50000);

  it('should add a follower in their followers list by the current user', () => {
    User.updateOne({ _id: process.env.userId },
      { $push: { Followers: testId } }, { new: true, safe: true })
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  }, 50000);
  it('should remove a follower from their followers list by the current user', () => {
    User.updateOne({ _id: process.env.userId },
      { $pull: { Followers: testId } }, { new: true, safe: true })
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  }, 50000);
});
