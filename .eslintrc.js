module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
    jest: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  rules: {
    'no-console': 0,
    'linebreak-style': 0,
    'no-underscore-dangle': 0,
    'consistent-return': 0,
    'max-len': 0,
    'no-shadow': 0,
  },
};
