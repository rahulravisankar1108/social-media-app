const mongoose = require('mongoose');

const { Schema } = mongoose;

const postSchema = new Schema({
  Image: {
    type: String,
    default: 'no photo',
  },
  Caption: {
    type: String,
    default: '',
  },
  Location: {
    type: String,
    default: '',
  },
  postedBy: {
    type: Schema.Types.ObjectId,
    ref: 'users',
    required: true,
  },
  likedBy: [{
    type: Schema.Types.ObjectId,
    ref: 'users',
  }],
  comments: [{
    type: Schema.Types.ObjectId,
    ref: 'comments',
  }],
  postedAt: {
    type: Date,
    default: Date.now,
  },
}, { timestamps: true });

const Posts = mongoose.model('posts', postSchema);
module.exports = { Posts };
