const express = require('express');
const Auth = require('../middleware/AuthLogin');

const router = express.Router();

const { removeUser, showUsers, countUsers } = require('../service/Follower');

router.post('/remove-user/:followerId', Auth, async (req, res) => {
  try {
    const response = await removeUser(req.user._id, req.params.followerId);

    return res.status(response.status).json({
      message: response.message,
      res: response.res,
    });
  } catch (err) {
    res.json({
      message: 'Controller follower Error',
      error: err,
    });
  }
});
router.get('/show-users', Auth, async (req, res) => {
  try {
    const response = await showUsers(req.user._id);

    return res.status(response.status).json({
      followerList: response.followersList,
      res: response.res,
    });
  } catch (err) {
    res.json({
      message: 'Find error Occured',
      error: err,
    });
  }
});
router.get('/count-users', Auth, async (req, res) => {
  try {
    const response = await countUsers(req.user._id);
    return res.status(response.status).json({
      followersCount: response.FollowersCount,
      res: response.res,
    });
  } catch (err) {
    res.json({
      message: 'Count error occured',
      error: err,
    });
  }
});
module.exports = router;
