const User = require('../models/User.model');

exports.addUser = async (userId, requestedUserId) => {
  try {
    await User.updateOne(
      { _id: requestedUserId },
      { $push: { Following: userId } },
      { safe: true },
    );
    await User.updateOne(
      { _id: userId },
      { $push: { Followers: requestedUserId } },
      { safe: true },
    );
    await User.updateOne(
      { _id: userId },
      { $pull: { Request: requestedUserId } },
      { safe: true },
    );
    await User.updateOne(
      { _id: requestedUserId },
      { $pull: { GivenRequest: userId } },
      { safe: true },
    );

    const response = {
      Message: 'Added Followers, Following',
      res: true,
      status: 200,
    };
    return response;
  } catch (err) {
    const response = {
      Message: 'An Error Occured!',
      res: false,
      status: 500,
    };
    return response;
  }
};

exports.removeUsers = async (userId, followingId) => {
  try {
    const deletedUser = await User.updateOne(
      { _id: userId },
      { $pull: { Following: followingId } },
      { safe: true },
    );
    if (deletedUser) {
      const response = {
        Message: 'Deleted the Following ID',
        res: true,
        status: 200,
      };
      return response;
    }
    const response = {
      Message: 'Mo user found',
      res: false,
      status: 201,
    };
    return response;
  } catch (err) {
    const response = {
      Message: 'Unexpected Deletion Error',
      res: false,
      status: 500,
    };
    return response;
  }
};

exports.showUser = async (userId) => {
  try {
    const userDetails = await User.findById(userId, { Following: 1 })
      .populate('Following', '_id UserName Email FullName Bio Website Gender ProfilePicture');
    if (userDetails) {
      const response = {
        Message: 'Following List',
        res: true,
        status: 200,
        FollowingList: userDetails,
      };
      return response;
    }
    const response = {
      Message: 'No Following List',
      res: false,
      status: 201,
      FollowingList: null,
    };
    return response;
  } catch (err) {
    const response = {
      Message: 'Error',
      res: false,
      status: 500,
      FollowingList: null,
    };
    return response;
  }
};

exports.countUsers = async (userId) => {
  try {
    const currentUser = await User.findById(userId, { Following: 1 });
    if (currentUser) {
      const count = currentUser.Following.length;

      const response = {
        res: true,
        Message: 'Following Count',
        FollowingCount: count,
        status: 200,
      };
      return response;
    }
    const response = {
      res: false,
      Message: 'No Following Count',
      FollowingCount: null,
      status: 201,
    };
    return response;
  } catch (err) {
    const response = {
      res: false,
      Message: 'Count error occured',
      FollowingCount: null,
      status: 500,
    };
    return response;
  }
};
