const User = require('../models/User.model');

exports.initiate = async (targetId, requestId) => {
  try {
    await User.updateOne({ _id: targetId }, { $push: { Request: requestId } }, { safe: true });
    await User.updateOne({ _id: requestId }, { $push: { GivenRequest: targetId } }, { safe: true });

    const response = {
      Message: 'Updated Request, GivenRequest',
      status: 200,
    };
    return response;
  } catch (err) {
    const response = {
      Message: 'An Error Occured',
      status: 404,
    };
    return response;
  }
};

exports.removeUser = async (userId, requestId) => {
  try {
    await User.updateOne({ _id: userId }, { $pull: { Request: requestId } }, { safe: true });
    await User.findOneAndUpdate(
      { _id: requestId },
      { $pull: { GivenRequest: userId } },
      { safe: true },
    );
    const response = {
      Message: 'Deleted the Selected Request ID',
      status: 200,
    };
    return response;
  } catch (err) {
    const response = {
      Message: 'Deletion Error for the Selected Request ID',
      status: 404,
    };
    return response;
  }
};

exports.showUser = async (userId) => {
  try {
    const currentUser = await User.findById(userId, { Request: 1 })
      .populate('Request', '_id UserName Email Phone Bio Website Gender ProfilePicture');
    if (currentUser) {
      const response = {
        status: 200,

        Message: 'requested user details',
        res: true,
        User: currentUser,
      };
      return response;
    }

    const response = {
      status: 201,
      Message: 'requested user details',
      res: false,
      User: false,
    };
    return response;
  } catch (err) {
    const response = {
      status: 500,
      Message: 'requested user Error!',
      res: false,
      User: false,
    };
    return response;
  }
};

exports.countUser = async (userId) => {
  try {
    const user = await User.findById(userId, { Request: 1 });
    const response = {
      status: 200,
      res: true,
      Count: user.Request.length,
      Message: 'Requests Count',
    };
    return response;
  } catch (err) {
    const response = {
      status: 500,
      res: false,
      Count: 0,
      Message: 'Count error occured',
    };
    return response;
  }
};

exports.clearUser = async (userId) => {
  try {
    const user = await User.findByIdAndUpdate(
      userId,
      { $set: { Request: [] } },
      { safe: true, new: true, upsert: true },
    );
    const response = {
      Message: 'Cleared',
      User: user,
      res: true,
      status: 200,
    };
    return response;
  } catch (err) {
    const response = {
      Message: err,
      User: 'Error',
      res: false,
      status: 500,
    };
    return response;
  }
};
