const mongoose = require('mongoose');

const { Schema } = mongoose;

const commentSchema = new Schema({
  message: {
    type: String,
    default: '',
  },
  postedBy: {
    type: Schema.Types.ObjectId,
    ref: 'users',
    required: true,
  },
  postedAt: {
    type: Date,
    default: Date.now,
  },
  likes: [{
    type: Schema.Types.ObjectId,
    ref: 'users',
  }],
}, { timestamps: true });

const Comment = mongoose.model('comments', commentSchema);
module.exports = Comment;
