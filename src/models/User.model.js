const mongoose = require('mongoose');

const { Schema } = mongoose;

const userDetailsSchema = Schema({
  email: {
    type: String,
    required: true,
  },
  userName: {
    type: String,
    required: true,
  },
  fullName: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
    required: true,
  },
  bio: {
    type: String,
    default: '',
  },
  website: {
    type: String,
    default: '',
  },
  gender: {
    type: String,
    default: '',
  },
  profilePicture: {
    type: String,
    default: 'no photo',
  },
  followers: [{
    type: Schema.Types.ObjectId,
    ref: 'users',
  }],
  following: [{
    type: Schema.Types.ObjectId,
    ref: 'users',
  }],
  request: [{
    type: Schema.Types.ObjectId,
    ref: 'users',
  }],
  givenRequest: [{
    type: Schema.Types.ObjectId,
    ref: 'users',
  }],
  savedPosts: [{
    type: Schema.Types.ObjectId,
    ref: 'Posts',
  }],
});

const User = mongoose.model('users', userDetailsSchema);
module.exports = User;
