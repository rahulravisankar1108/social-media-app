const { LocalStorage } = require('node-localstorage');
const assert = require('assert');
const {
  dbConnect,
  dbDisconnect,
} = require('../utils/test-utils/dbHandler.utils');
require('dotenv').config();

const localStorage = new LocalStorage('./scratch');

const User = require('../../src/models/User.model');

const testId = localStorage.getItem('testId');
let done;
let res;

describe('Unit Tests : Test for Routes/Request.js:', () => {
  beforeEach(() => {
    jest.setTimeout(50000);
  });

  beforeAll(async () => {
    dbConnect();
  });

  afterAll(async () => {
    dbDisconnect();
    await new Promise((resolve) => setTimeout(() => resolve(), 10000));
  });

  it('should find all the Request from the other user to the current user', () => {
    User.findById(process.env.userId)
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  }, 50000);

  it('should initiate a request to other user from current user', () => {
    User.updateOne({ _id: process.env.userId },
      { $push: { GivenRequest: testId } }, { new: true, safe: true })
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  }, 50000);

  it('should delete a request sent by current user to other user', () => {
    User.updateOne({ _id: process.env.userId },
      { $pull: { GivenRequest: testId } }, { new: true, safe: true })
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  }, 50000);
});
