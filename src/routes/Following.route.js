const express = require('express');
const Auth = require('../middleware/AuthLogin');

const router = express.Router();

const {
  addUser, removeUsers, showUser, countUsers,
} = require('../service/Following');

router.get('/accept-user/:Id', Auth, async (req, res) => {
  try {
    const response = await addUser(req.user._id, req.params.Id);

    return res.status(response.status).json({
      message: response.message,
      res: response.res,
    });
  } catch (err) {
    return res.status(500).json({
      message: 'An Error Occured!',
      error: err,
    });
  }
});
router.post('/remove-user/:Id', Auth, async (req, res) => {
  try {
    const response = await removeUsers(req.user._id, req.params.Id);

    return res.status(response.status).json({
      message: response.message,
      res: response.res,
    });
  } catch (err) {
    res.status(500).json({
      message: 'Deletion Error',
      error: err,
    });
  }
});
router.get('/show-users', Auth, async (req, res) => {
  try {
    const response = await showUser(req.user._id);
    return res.status(response.status).json({
      followingLis: response.followingList,
      res: response.res,
      message: response.message,
    });
  } catch (err) {
    res.json({
      message: 'Find error Occured',
      error: err,
    });
  }
});
router.get('/count-users', Auth, async (req, res) => {
  try {
    const response = await countUsers(req.user._id);

    return res.status(response.status).json({
      followingUserCount: response.followingCount,
      res: response.res,
      message: response.message,
    });
  } catch (err) {
    res.status(500).json({
      message: 'Count error occured',
    });
  }
});

module.exports = router;
