const express = require('express');

const { signUp, login } = require('../service/Auth');
const { loginValidation, signUpValidation } = require('../validations/Auth.validation');

const router = express.Router();

router.post('/login', loginValidation, async (req, res) => {
  try {
    const { email } = req.body;

    const response = await login(email);
    return res.status(response.status).json({
      message: response.Message,
      res: response.res,
      Token: response.Token,
      User: response.User,
    });
  } catch (err) {
    return res.status(500).json({
      message: 'Error Occured!',
      Error: err,
    });
  }
});

router.post('/signup', signUpValidation, async (req, res) => {
  try {
    const {
      email, userName, fullName, password, phone,
    } = req.body;

    const response = await signUp(email, fullName, userName, password, phone);
    return res.status(response.status).json({
      message: response.Message,
      res: response.res,
    });
  } catch (err) {
    return res.status(500).json({
      message: 'Error Occured!',
      Error: err,
    });
  }
});

module.exports = router;
