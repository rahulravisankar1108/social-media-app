const express = require('express');
const Auth = require('../middleware/AuthLogin');

const router = express.Router();
const {
  add, deleteComment, like, unLike,
} = require('../service/Comment');

router.post('/add/:postId', Auth, async (req, res) => {
  try {
    const {
      message,
    } = req.body;

    const response = await add(req.params.postId, message, req.user_id);
    return res.status(response.status).json({
      res: response.res,
    });
  } catch (err) {
    res.status(500).json({
      res: true,
    });
  }
});
router.delete('/remove/:commentId', Auth, async (req, res) => {
  try {
    const {
      postId,
    } = req.body;

    const response = await deleteComment(postId, req.params.commentId);
    res.status(response.status).json({
      res: response.res,
      message: response.message,
    });
  } catch (err) {
    res.status(500).json({
      res: true,
    });
  }
});

router.put('/like/:commentId', Auth, async (req, res) => {
  try {
    const response = await like(req.params.commentId, req.user_id);
    if (response) {
      return res.status(response.status).json({
        message: response.comment,
        res: response.res,
      });
    }
  } catch (err) {
    res.status(500).json({
      Error: 'Error Occured in like comment.',
    });
  }
});

router.put('/unlike/:commentId', Auth, async (req, res) => {
  try {
    const response = await unLike(req.params.commentId, req.user_id);
    if (response) {
      return res.status().json({
        message: response.comment,
        res: response.res,
      });
    }
  } catch (err) {
    res.status(500).json({
      Error: 'Error Occured in like comment.',
    });
  }
});

module.exports = router;
