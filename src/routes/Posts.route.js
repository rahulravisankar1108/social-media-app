const express = require('express');

const router = express.Router();
const Auth = require('../middleware/AuthLogin');
const { storePostValidator, updatePostValidator } = require('../validations/Post.validation');
const {
  getFriendsPost, storePost, unlikePost, likePost,
  updatePostDetails, getUserPost,
  removePosts, removePost,
} = require('../service/Posts');

router.get('/friends-post', Auth, async (req, res) => {
  try {
    const response = await getFriendsPost(req.user._id);
    return res.status(response.status).json({
      friendPost: response.friendPost,
      res: response.res,
      message: response.message,
    });
  } catch (err) {
    return res.status(500).json(err);
  }
});
router.post('/store-post', Auth, storePostValidator, async (req, res) => {
  try {
    const { location, caption } = req.body;
    const image = req.file.path;
    const response = await storePost(req.user._id, location, image, caption);

    return res.status(response.status).json({
      message: response.message,
      res: response.res,
    });
  } catch (err) {
    res.status(500).json({
      message: 'An error Occured',
      Error: err,
    });
  }
});

router.get('/all-post', Auth, async (req, res) => {
  try {
    const response = await getUserPost(req.user._id);
    return res.status(response.status).json({
      res: response.res,
      postDetails: response.postDetails,
    });
  } catch (err) {
    res.status(500).json({
      message: 'An error Occured',
      Error: err,
    });
  }
});

router.put('/update-post-Details', Auth, updatePostValidator, async (req, res) => {
  try {
    const { caption, location } = req.body;
    const response = await updatePostDetails(req.user._id, caption, location);
    res.status(response.status).json({
      message: response.message,
      res: response.res,
    });
  } catch (err) {
    res.status(500).json({
      message: 'An error Occured',
      Error: err,
    });
  }
});
router.delete('/remove-post/:id', Auth, async (req, res) => {
  try {
    const response = await removePost(req.params.id);
    res.status(response.status).json({
      message: response.message,
      res: response.message,
    });
  } catch (err) {
    res.status(500).json({
      message: 'An error Occured',
      Error: err,
    });
  }
});
router.delete('/remove-all-post', Auth, async (req, res) => {
  try {
    const response = await removePosts(req.user_id);
    res.status(response.status).json({
      message: response.message,
      res: response.res,
    });
  } catch (err) {
    res.status(500).json({
      message: 'Error',
      Error: err,
    });
  }
});
router.put('/like-post/:postId', Auth, async (req, res) => {
  try {
    const response = await likePost(req.params.postId, req.user_id);
    res.status(response.status).json({
      message: response.message,
      res: response.res,
    });
  } catch (err) {
    return res.status(500).json({
      Error: err,
    });
  }
});
router.put('/unlike-post/:postId', Auth, async (req, res) => {
  try {
    const response = await unlikePost(req.params.postId, req.user_id);

    res.status(response.status).json({
      message: response.message,
      res: response.res,
    });
  } catch (err) {
    return res.status(500).json({
      Error: err,
    });
  }
});

module.exports = router;
