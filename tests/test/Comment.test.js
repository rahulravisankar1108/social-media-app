const { LocalStorage } = require('node-localstorage');
const { v4: uuidv4 } = require('uuid');
const assert = require('assert');
const {
  dbConnect,
  dbDisconnect,
} = require('../utils/test-utils/dbHandler.utils');
require('dotenv').config();

const localStorage = new LocalStorage('./scratch');

const Comment = require('../../src/models/Comment.model');
const { Posts } = require('../../src/models/Posts.model');

let done;
let res;

describe('Unit Tests : Test for Routes/Comment.js:', () => {
  beforeEach(() => {
    jest.setTimeout(50000);
  });

  beforeAll(async () => {
    dbConnect();
  });

  afterAll(async () => {
    dbDisconnect();
    await new Promise((resolve) => setTimeout(() => resolve(), 10000));
  });

  it('should add post comment of the existing user from other user', () => {
    const newComment = new Comment({
      message: `${uuidv4()}`,
      postedBy: process.env.userId,
    });
    newComment.save();
    Comment.findById(newComment)
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  }, 50000);

  it('should display all the comments of the post', () => {
    Posts.findById(localStorage.getItem('postId'), { comments: 1 })
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  }, 50000);

  it('should delete all the comments of the post', () => {
    Posts.updateOne(localStorage.getItem('postId'), { $set: { comments: [] } }, { new: true, safe: true })
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  }, 50000);
});
