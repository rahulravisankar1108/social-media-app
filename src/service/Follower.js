const User = require('../models/User.model');

exports.removeUser = async (userId, followerId) => {
  try {
    const updatedUser = await User.updateOne(
      { _id: userId },
      { $pull: { Followers: followerId } },
      { safe: true },
    );
    if (updatedUser) {
      const response = {
        Message: 'Deleted the ID',
        res: true,
        status: 200,
      };
      return response;
    }
    const response = {
      Message: 'User not found',
      res: false,
      status: 201,
    };
    return response;
  } catch (err) {
    const response = {
      Message: 'Deletion Error',
      res: false,
      status: 500,
    };
    return response;
  }
};

exports.showUsers = async (userId) => {
  try {
    const currentUser = await User.findById(userId, { Followers: 1 })
      .populate('Followers', '_id UserName Email FullName Phone Bio Website Gender ProfilePicture');
    const response = {
      res: true,
      followersList: currentUser,
      status: 200,
    };
    return response;
  } catch (err) {
    const response = {
      res: false,
      followersList: null,
      status: 500,
    };
    return response;
  }
};

exports.countUsers = async (userId) => {
  try {
    const currentUser = await User.findById(userId);
    const response = {
      res: true,
      followersCount: currentUser.Followers.length,
      status: 200,
    };
    return response;
  } catch (err) {
    const response = {
      res: false,
      FollowersCount: null,
      status: 500,
    };
    return response;
  }
};
